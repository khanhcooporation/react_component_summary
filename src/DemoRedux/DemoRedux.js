import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { rootReducer } from "../Store/rootReducer";
const DemoRedux = () => {
  // useSelector: Dùng để lấy giá trị ở store của redux
  const demoRedux = useSelector((state) => {
    return state.demoRedux;
  });
  const dispatch = useDispatch();
  return (
    <div className="container mt-5">
      <h1>DemoRedux</h1>
      <p className="display-4">Number: {demoRedux.number}</p>
      <p className="display-4">Name: {demoRedux.name}</p>
      <div>
        <button
          className="btn btn-success"
          onClick={() => {
            dispatch({ type: "INCREA_NUMBER", payload: 2 });
          }}
        >
          Increa Number
        </button>
      </div>
    </div>
  );
};

export default DemoRedux;
