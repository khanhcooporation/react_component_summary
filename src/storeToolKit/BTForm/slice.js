import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  productList: [],
  productEdit: undefined,
};

const btFormSlice = createSlice({
  name: "btForm",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.productList.push(action.payload);
    },
    deleteProduct: (state, action) => {
      //action.payload = id của product muốn xóa
      state.productList = state.productList.filter(
        (prd) => prd.id !== action.payload
      );
    },
    editProduct: (state, action) => {
      //action.payload =product
      state.productEdit = action.payload;
    },
    updateProduct: (state, action) => {
      // action.payload là 1 obj product edit
      console.log("yess");
      state.productList = state.productList.map((prd) => {
        if (prd.id === action.payload.id) {
          return action.payload;
        }
        return prd;
      });
      console.log("state.productList", state.productList);
      // sau khi update thành công
      state.productEdit = undefined;
    },
  },
});

export const { actions: btFormActions, reducer: btFormReducer } = btFormSlice;
