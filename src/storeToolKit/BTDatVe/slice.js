import { createSlice } from "@reduxjs/toolkit";

//Rộng hơn, chứa reducer
//*Tự động tạo action & reducer
const initialState = {
  //Những gì bỏ vào đây thì khai báo g trị mặc định của state đó ( Bật f12 có thể coi)
  //Muốn đổi thì sd dispatch
  number: 100,
  //*=====================================
  chairBooking: [],
  chairBooked: [],
};

const BTDatVeSlice = createSlice({
  name: "BTDatVe", //Đặt theo folder
  initialState, //?==initialState: initialState =>>> Object literal
  //!    v Chú ý
  reducers: {
    //*============================================================
    setNumber: (state, action) => {
      console.log("action: ", action);
      state.number = action.payload; //!State.tênstate= action.payload
    },
    //? Nó cũng chính là action => switch case trog đây
    //?Type sẽ có dạng name/phươngthức()
    //*=============================================================
    setChairBooking: (state, action) => {
      //KTra xem có tồn tại ghế trong mảng chairBooking ko?
      const index = state.chairBooking.findIndex(
        (e) => e.soGhe == action.payload.soGhe
      );
      if (index != -1) {
        //Nếu tồn tại thì xóa khỏi mảng chairBooking
        state.chairBooking.splice(index, 1);
      } else {
        //Nếu chưa tồn tại thì push vào
        state.chairBooking.push(action.payload);
      }
    },
    setChairBooked: (state, action) => {
      state.chairBooked = [...state.chairBooked, ...state.chairBooking];
      state.chairBooking = [];
    },
  },
});
//                                          //!vvv Chú ý cú pháp
export const { actions: baiTapDatVeActions, reducer: baiTapDatVeReducer } =
  BTDatVeSlice; //Trả về 2 kiểu: action & reducer
//                        //? reducer : <-- để đặt tên cho reducer
