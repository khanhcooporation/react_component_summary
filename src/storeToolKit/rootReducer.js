import { combineReducers } from "redux";
import demoReduxReducer from "../Store/demoRedux/reducer";
import { btPhoneReducer } from "../Store/BTphoneRedux/reducer";
import { btDatveReducer } from "../Store/BTDatVe/reducer";
import { BTDatVeReducer, baiTapDatVeReducer } from "./BTDatVe/slice";
import { btFormReducer } from "./BTForm/slice";

export const rootReducer = combineReducers({
  // reducer của store redux cũ
  demoRedux: demoReduxReducer,
  btPhone: btPhoneReducer,
  btDatVe: btDatveReducer,

  // reducer của redux toolkit
  btDatVeToolkit: baiTapDatVeReducer,
  btForm: btFormReducer,
});
