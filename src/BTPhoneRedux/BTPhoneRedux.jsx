// rafce
import React from 'react'
import data from './data.json'
import ProductDetail from './ProductDetail'
import GioHang from './GioHang'
import ProductList from './ProductList'

const BTPhoneRedux = () => {
    return (
        <div className="container mt-5">
            <div className="d-flex justify-content-between mb-5">
                <h1>BTPhoneRedux</h1>
                <button className="btn btn-success" data-toggle="modal" data-target="#gioHangRedux">
                    Giỏ hàng
                </button>
            </div>

            <ProductList data={data} />

            <ProductDetail />

            <GioHang />
        </div>
    )
}

export default BTPhoneRedux
