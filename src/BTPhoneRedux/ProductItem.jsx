import React from "react";
import { useDispatch } from "react-redux";
import {
  HANDLE_CARTS,
  HANDLE_PRODUCTDETAIL,
} from "../Store/BTphoneRedux/actionType";
import { cartsAction, productDetailAction } from "../Store/BTphoneRedux/action";

const ProductItem = ({ product }) => {
  // dùng để thay đổi giá trị state trên store của redux
  const dispatch = useDispatch();

  return (
    <div className="col-4">
      <div className="card">
        <div>
          <img
            style={{ width: 350, height: 350 }}
            className="img-fluid"
            src={product.hinhAnh}
            alt="..."
          />
        </div>
        <div className="card-body">
          <p>
            <b>{product.tenSP}</b>
          </p>
          <button
            className="btn btn-success mt-3"
            onClick={() => {
              // dispatch({
              //     type: HANDLE_PRODUCTDETAIL,
              //     payload: product,
              // })
              dispatch(productDetailAction(product));
            }}
          >
            Xem chi tiết
          </button>
          <button
            className="btn btn-warning mt-3 ml-3"
            onClick={() => {
              // dispatch({
              //     type: HANDLE_CARTS,
              //     payload: product,
              // })
              dispatch(cartsAction(product));
            }}
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
