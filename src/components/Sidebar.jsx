import React from "react";
import { Link, NavLink } from "react-router-dom";
import { PATH } from "../config/path";
import "./style.scss";

const Sidebar = () => {
  return (
    //?Với NavLink, thẻ sẽ tự động thêm class active nếu trùng đường dẫn
    //?Với Link,
    <div className="d-flex flex-column">
      <Link to={PATH.databinding}>DataBinding</Link>
      <NavLink to={PATH.databindingwithcondition}>
        databindingwithcondition
      </NavLink>
      <NavLink to={PATH.handleEvent}>handleEvent</NavLink>
      <NavLink to={PATH.renderWithMap}>renderWithMap</NavLink>
      <NavLink to={PATH.btMovie}>btMovie</NavLink>
      <NavLink to={PATH.styleComponent}>styleComponent</NavLink>
      <NavLink to={PATH.demoState}>demoState</NavLink>
      <NavLink to={PATH.demoProps}>demoProps</NavLink>
      <NavLink to={PATH.btShoes}>btShoes</NavLink>
      <NavLink to={PATH.btPhones}>btPhones</NavLink>
      <NavLink to={PATH.redux}>demoRedux</NavLink>
      <NavLink to={PATH.btPhoneRedux}>btPhoneRedux</NavLink>
      <NavLink to={PATH.btDatVe}>btDatVe</NavLink>
      <NavLink to={PATH.btDatVeToolkit}>btDatVeToolkit</NavLink>
      <NavLink to={PATH.btForm}>btForm</NavLink>
      <NavLink to={PATH.useEffect}>useEffect</NavLink>
      <NavLink to={PATH.btTaskManager}>btTaskManager</NavLink>
      <NavLink to={PATH.demoHooks}>Hooks</NavLink>
      <NavLink to={PATH.lifeCycle}>LifeCycle</NavLink>
    </div>
  );
};

export default Sidebar;
