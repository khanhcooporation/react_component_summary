//rafce
import React from 'react'

const DataBindingWithCondition = () => {
    const isShowMessage = false

    const student = {}

    const message = 'Hello BC46'

    return (
        <div className="container mt-5">
            DataBindingWithCondition
            {/* <p>{isShowMessage ? message : 'Hello Cybersoft'}</p> */}

            <p>{isShowMessage && message}</p>

            <p>{!!student && <p>Hello</p>}</p>
        </div>
    )
}

export default DataBindingWithCondition


// falsy: NaN, undefined , null, 0, '', false
