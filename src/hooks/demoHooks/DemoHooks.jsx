import React, { useEffect, useRef, useState } from "react";
import { useMemo, useCallback } from "react";
import Childrent from "./Childrent";

const DemoHooks = () => {
  const [number, setNumber] = useState(1);
  const [like, setLike] = useState(1);

  //   const sum = number + 100;
  const sum = useMemo(() => {
    console.log("useMemo");
    return number + 100;
  }, [number]);

  //* useCallback chỉ có tác dụng khi component nhận props phải sử dụng memo
  const handleLike = useCallback(() => {
    setLike(like + 1);
  }, [like]); // #123 //!Coi lại cho hiểu

  //*Chức năng 1: DOM đến 1 gtrị.
  const inputRef = useRef(null);
  console.log("inputRef: ", inputRef.current);

  const object = {
    number: 10,
  }; //#123  =>>>Rerender: =>> #1234

  const objectRef = useRef({
    number: 10,
  });

  object.number = 100;

  //*Muốn log useRef => Phải sd objectRef.current <<<==
  objectRef.current.number = 100;
  console.log("object: ", object);
  console.log(("objectRef: ", objectRef));

  useEffect(() => {
    console.dir(inputRef.current);
  }, []);

  //! Chỉ gọi khi number THAY ĐỔI
  return (
    <div>
      <h1>DemoHooks</h1>
      <div className="mt-3">
        <p>Number: {number}</p>
        <div className="mt-3">
          <button
            className="btn btn-success"
            onClick={() => {
              setNumber(number + 1);
              object.number = 100;
              objectRef.current.number = 100;
            }}
          >
            + Number
          </button>
        </div>
        <p className="my-3">like: {like}</p>
        <div className="mt-3">
          <button
            className="btn btn-success"
            onClick={() => {
              setLike(like + 1);
            }}
          >
            + Like
          </button>
        </div>
        <p>sum: {sum}</p>

        <div className="mt-5">
          <Childrent like={like} handleLike={handleLike} />
        </div>
      </div>

      <div className="mt-5">
        <input className="form-control" ref={inputRef} />
      </div>
    </div>
  );
};

export default DemoHooks;
