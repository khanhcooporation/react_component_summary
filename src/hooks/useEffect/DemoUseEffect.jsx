import React, { useEffect, useState } from "react";

const DemoUseEffect = () => {
  const [number, setNumber] = useState(1);
  const [like, setLike] = useState(1);
  //Cú pháp:
  //! useEffect(() => {}, []);
  //Nhận 2 tham số: param1: Callback ;   param2 (optional): [] có thể có hoặc ko
  //*TH1: luôn chạy mỗi khi Component render => ít sd
  //!Chạy sau khi component return (render xong UI)
  useEffect(() => {
    console.log("UseEffect TH1: ");
  });
  //console.log("Render");
  //Render sẽ luôn log ra trước

  //*TH2: Chạy DUY NHẤT 01 LẦN sau khi Component render
  useEffect(() => {
    console.log("UseEffect TH2:");
  }, []);

  //*TH3: Chạy lại mỗi khi number thay đổi
  useEffect(() => {
    console.log("UseEffect TH3:");
  }, [number]);

  //*TH4: Thường được sử dụng dể clean up component,
  //!chạy sau khi Component bị hủy khỏi giao diện
  // useEffect(() => {
  //   const time = setInterval(() => {
  //     console.log("Hello World");
  //   }, 1000);
  //   //*Hàm return sẽ đc gọi khi Component bị hủy khỏi giao diện (UI)
  //   return () => {
  //     clearInterval(time);
  //   };
  // });

  const [provinceId, setProvinceId] = useState();
  const [districts, setDistricts] = useState([]);
  console.log("districts: ", districts);

  useEffect(() => {
    console.log("provinceId", provinceId);
    if (provinceId && provinceId !== "none") {
      const districts = address.find(
        (value) => value.id === provinceId
      ).district;
      setDistricts(districts);
    }
  }, [provinceId]);

  const address = [
    {
      province: "Hồ Chí Minh",
      id: "HCM",
      district: [
        {
          id: "HCM01",
          name: "Quận 1",
        },
        {
          id: "HCM02",
          name: "Quận 2",
        },
        {
          id: "HCM03",
          name: "Quận 3",
        },
      ],
    },
    {
      province: "Hà Nội",
      id: "HN",
      district: [
        {
          id: "HN01",
          name: "Quận Hoàn Kiếm",
        },
        {
          id: "HN02",
          name: "Quận Hai Bà Trưng",
        },
        {
          id: "HN03",
          name: "Quận Đống Đa",
        },
      ],
    },
  ];

  //TODO: =>>> TH3 Có thể set cho chạy lại ở một số State cố định; còn TH1 sẽ luôn chạy lại Ở MỌI STATE
  //! useEffect sẽ luôn chạy ít nhất một lần với tất cả các trường hợp.
  return (
    //*UseEffect được dùng dể thực hiện các side Effect: Call API, setState, cleanup, tương tác với DOM
    <div>
      <h1>DemoUseEffect</h1>

      <div className="mt-3">
        <p>Number: {number}</p>
        <button
          className="btn btn-success mt-3"
          onClick={() => {
            setNumber(number + 1);
          }}
        >
          +Number
        </button>
      </div>

      <div className="mt-3">
        <p>Number: {number}</p>
        <button
          className="btn btn-success mt-3"
          onClick={() => {
            setLike(like + 1);
          }}
        >
          +Like
        </button>
      </div>

      <div className="">
        <p>Example</p>
        <div className="">
          <select
            name=""
            id=""
            value={provinceId}
            onChange={(ev) => {
              console.log(ev.target.value);
              setProvinceId(ev.target.value);
            }}
          >
            <option value="none">Vui lòng chọn tỉnh/thành phố</option>
            {address.map((e) => (
              <option value={e.id} key={e.id}>
                {e.province}
              </option>
            ))}
            {/* <option value="HCM">Hồ Chí Minh</option>
            <option value="HN">Hà Nội</option> */}
          </select>
          <select name="" id="" className="form-control mt-3">
            <option value="">Vui lòng chọn quận/huyện</option>
            {districts?.map((district) => (
              <option value={district.id} key={district.id}>
                {district.name}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
};

export default DemoUseEffect;
