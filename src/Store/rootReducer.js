import { combineReducers } from "redux";
import demoReduxReducer from "./demoRedux/reducer";
import { btPhoneReducer } from "./BTphoneRedux/reducer";
import { btDatveReducer } from "./BTDatVe/reducer";

export const rootReducer = combineReducers({
  demoRedux: demoReduxReducer,
  btPhone: btPhoneReducer,
  btDatVe: btDatveReducer,
});
