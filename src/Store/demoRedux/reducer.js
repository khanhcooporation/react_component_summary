const initialState = {
  number: 1,
  name: "abc",
};
const demoReduxReducer = (state = initialState, action) => {
  console.log("🚀action:", action);
  switch (action.type) {
    case "INCREA_NUMBER":
      const data = { ...state };
      data.number += action.payload;
      return data;
    //*   return { ...state, number: state.number + action.payload };
    default:
      return state;
  }
};

export default demoReduxReducer;
