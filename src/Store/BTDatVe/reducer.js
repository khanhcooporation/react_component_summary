import { HANDLE_CHAIR_BOOKINGS, PAY } from "./actionType";

const initialState = {
  chairBookings: [],
  chairBookeds: [],
};
export const btDatveReducer = (state = initialState, { type, payload }) => {
  console.log("payload: ", payload);
  switch (type) {
    case HANDLE_CHAIR_BOOKINGS: {
      const data = [...state.chairBookings]; ///Clone cũ
      //!ktra ghế được click đã tồn tại trong mảng đang chọn hay không?
      const index = data.findIndex((value) => value.soGhe == payload.soGhe);
      if (index === -1) {
        data.push(payload);
      } else {
        data.splice(index, 1);
      }
      ///Chưa có thì Push mới có thì splice đi
      return { ...state, chairBookings: data };
    }
    case PAY: {
      const data = [...state.chairBookeds, ...state.chairBookings];
      return { ...state, chairBookeds: data, chairBookings: [] };
    }
    default:
      return state;
  }
};
