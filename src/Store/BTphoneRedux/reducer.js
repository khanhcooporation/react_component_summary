import {
  HANDLEQUANTITY,
  HANDLE_CARTS,
  HANDLE_PRODUCTDETAIL,
} from "./actionType";

const initialState = {
  productDetail: {
    maSP: 1,
    tenSP: "VinSmart Live",
    manHinh: "AMOLED, 6.2, Full HD+",
    heDieuHanh: "Android 9.0 (Pie)",
    cameraTruoc: "20 MP",
    cameraSau: "Chính 48 MP & Phụ 8 MP, 5 MP",
    ram: "4 GB",
    rom: "64 GB",
    giaBan: 5700000,
    hinhAnh: "./images/phones/vsphone.jpg",
  },
  cart: [
    {
      maSP: 1,
      tenSP: "VinSmart Live",
      manHinh: "AMOLED, 6.2, Full HD+",
      heDieuHanh: "Android 9.0 (Pie)",
      cameraTruoc: "20 MP",
      cameraSau: "Chính 48 MP & Phụ 8 MP, 5 MP",
      ram: "4 GB",
      rom: "64 GB",
      giaBan: 5700000,
      hinhAnh: "./images/phones/vsphone.jpg",
      soLuong: 1,
    },
  ],
};

export const btPhoneReducer = (state = initialState, action) => {
  console.log("action: ", action);
  switch (action.type) {
    case HANDLE_PRODUCTDETAIL:
      return { ...state, productDetail: { ...action.payload } };
    case HANDLE_CARTS:
      let newCarts = [...state.cart];
      //*Tìm kiếm trong cart xem có sp trùng với sp gửi lên hay không?
      //? có sẽ trả vị trí, else -1
      const index = state.cart.findIndex(
        (prd) => prd.maSP === action.payload.maSP
      ); // -1

      if (index == -1) {
        newCarts.push({
          ...action.payload,
          soLuong: 1,
        });
      } else {
        newCarts[index].soLuong += 1;
      }

      return { ...state, cart: newCarts }; //!!!!! VÌ SAO PHẢI cart: newCarts?
    //??? VÌ để khi action payload, cart vẫn là 1 Array để có thể map
    case HANDLEQUANTITY: {
      let newCart = [...state.cart];
      newCart = newCart.map((cart) => {
        if (cart.maSP == action.payload.maSP) {
          return {
            ...cart,
            soLuong: (cart.soLuong += action.payload.quantity) || 1,
          };
        }
        return cart;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
