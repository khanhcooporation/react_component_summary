import {
  HANDLEQUANTITY,
  HANDLE_CARTS,
  HANDLE_PRODUCTDETAIL,
} from "./actionType";

export const productDetailAction = (payload) => {
  return {
    type: HANDLE_PRODUCTDETAIL,
    payload,
  };
};

export const cartsAction = (payload) => {
  return {
    type: HANDLE_CARTS,
    payload,
  };
};

export const quantityAction = (payload) => {
  return {
    type: HANDLEQUANTITY,
    payload,
  };
};
