/**
 *
 * @param : object
 * @returns - search string dùng để Query url
 */

//? JS DOC: Hover vào hàm export dứi để biết công dụng =]]]]
import qs from "qs";
export const generateSearchString = (params) => {
  const searchString = qs.stringify(params, { addQueryPrefix: true });
  return searchString;
};
