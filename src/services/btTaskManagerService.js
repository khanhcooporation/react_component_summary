import axios from "axios";
export const btTaskManagerServices = {
  getTaskList: () =>
    axios.get("http://svcy.myclass.vn/api/ToDoList/GetAllTask"),
  createTask: (payload) =>
    axios.post("http://svcy.myclass.vn/api/ToDoList/AddTask", payload),
  deleteTask: (taskName) => {
    axios.delete(
      `http://svcy.myclass.vn/api/ToDoList/deleteTask?taskName=${taskName}`
    );
  },
  updateStatusTask: (taskName) => {
    axios.put(
      `http://svcy.myclass.vn/api/ToDoList/doneTask?taskName=${taskName}`
    );
  },
};
