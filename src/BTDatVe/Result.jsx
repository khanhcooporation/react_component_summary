import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { payAction, chairBookingsAction } from "../Store/BTDatVe/action";

const Result = () => {
  const { chairBookings, chairBookeds } = useSelector((state) => state.btDatVe);
  const dispatch = useDispatch();
  console.log("chairBookings: ", chairBookings);
  console.log("chairBookeds", chairBookeds);
  return (
    <div>
      <h2>Danh sách ghế bạn chọn</h2>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark">Ghế đã đặt</button>
        <button className="btn btn-outline-dark mt-3 Chair booking">
          Ghế đang chọn
        </button>
        <button className="btn btn-outline-dark mt-3">Ghế chưa đặt</button>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {chairBookings.map((ghe) => (
            <tr>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => {
                    dispatch(chairBookingsAction(ghe));
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}

          {/* Tính tổng tiền */}
          <tr>
            <td>Tổng tiền</td>
            <td>
              {chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}
            </td>
          </tr>
        </tbody>
      </table>
      <button
        className="btn btn-success"
        onClick={() => {
          dispatch(payAction());
        }}
      >
        Thanh Toán
      </button>
    </div>
  );
};

export default Result;

// import React from "react";
// import { useDispatch, useSelector } from "react-redux";

// const Result = () => {
//   const { chairBookings } = useSelector((state) => state.btDatVe);
//   console.log("chairBookings: ", chairBookings);
//   return (
//     <div>
//       <h2>Danh sách ghế bạn chọn</h2>
//       <div className="d-flex flex-column">
//         <button className="btn btn-outline-dark">Ghế đã đặt</button>
//         <button className="btn btn-outline-dark mt-3">Ghế đang chọn</button>
//         <button className="btn btn-outline-dark mt-3">Ghế chưa đặt</button>
//       </div>

//       <table className="table">
//         <thead>
//           <tr>
//             <th>Số ghế</th>
//             <th>Giá</th>
//             <th>Hủy</th>
//           </tr>
//         </thead>
//         <tbody>
//           {chairBookings.map((ghe) => {
//             return (
//               <tr>
//                 <td>{ghe.soGhe}</td>
//                 <td>{ghe.gia}</td>
//                 <button className="btn btn-danger">Hủy</button>
//               </tr>
//             );
//           })}
//         </tbody>
//         {/* Tính tổng tiền */}
//         <tr>
//           <td>Tổng tiền</td>
//           <td>{chairBookings.reduce((ghe, total) => (total += ghe.gia), 0)}</td>
//         </tr>
//       </table>
//     </div>
//   );
// };

// export default Result;
