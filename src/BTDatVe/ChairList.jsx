import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  console.log(data);
  return (
    <div>
      {data.map((hangGhe) => {
        return (
          <div className="d-flex mt-3" style={{ gap: "10px" }}>
            <div className="text-center" style={{ width: "40px" }}>
              {hangGhe.hang}
            </div>
            {/* //*Mỗi hàng có ghế khác nhau nên map 1 lần nữa */}
            <div className="d-flex" style={{ gap: "10px" }}>
              {hangGhe.danhSachGhe.map((ghe) => {
                return <Chair ghe={ghe} key={ghe.soGhe}></Chair>;
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ChairList;
