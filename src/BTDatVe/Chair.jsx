import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingsAction } from "../Store/BTDatVe/action";
import cn from "classnames";
import "./style.scss";

const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  const { chairBookings, chairBookeds } = useSelector((state) => state.btDatVe);
  //   console.log("chairBookings: ", chairBookings);
  return (
    <button
      className={cn("btn btn-outline-dark Chair", {
        booking: chairBookings.find((e) => e.soGhe === ghe.soGhe),
        booked: chairBookeds.find((e) => e.soGhe === ghe.soGhe),
      })}
      style={{ width: "50px" }}
      onClick={() => {
        dispatch(chairBookingsAction(ghe));
      }}
    >
      {ghe.soGhe}
    </button>
  );
};

export default Chair;
