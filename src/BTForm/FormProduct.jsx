import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormActions } from "../storeToolKit/BTForm/slice";

const FormProduct = () => {
  const [formData, setFormData] = useState();
  const [formError, setFormError] = useState();
  // console.log("formData: ", formData);
  const dispatch = useDispatch();
  const { productEdit } = useSelector((state) => state.btForm);
  // console.log(
  //   "🚀 ~ file: FormProduct.jsx:12 ~ FormProduct ~ productEdit:",
  //   productEdit
  // );
  //   const handleFormData = (name, value) => {
  //     setFormData({
  //       ...formData,
  //       [name]: value,
  //     });
  //   };

  //Currying function
  const handleFormData = () => (ev) => {
    const { name, value, title, minLength, max, pattern, validity } = ev.target;
    // console.log(validity);
    // console.log("~ minLength:", minLength);
    // console.log("~FormError: ", formError);
    let mess;
    if (minLength != -1 && !value.length) {
      mess = `Vui Lòng nhập thông tin ${title}`;
    } else if (value.length < minLength) {
      mess = `vui lòng nhập tối thiểu ${minLength} ký tự`;
    } else if (value.length > max && max) {
      mess = `Chỉ được nhập tối đa ${max} ký tự`;
    } else if (
      validity.patternMismatch &&
      ["id", "price"].includes(name) ///?name chỉ có thể là id và price thì mới hợp lệ
    ) {
      mess = `Vui lòng nhập ký số`;
    }
    setFormError({
      ...formError,
      [name]: mess,
    });
    // console.log(mess);
    setFormData({
      ...formData,
      [name]: mess ? undefined : value,
    });
  };

  useEffect(() => {
    if (!productEdit) return;

    //*Dùng để bắt thay đổi, nó render lại sự thay đổi
    setFormData(productEdit);
  }, [productEdit]);

  //Button nằm trong thẻ form nếu k gắn thuộc tình type => mặc định là submit
  return (
    <form
      onSubmit={(event) => {
        //*Ngăn sự kiện reload trang khi submit

        event.preventDefault();

        const elements = document.querySelectorAll("input");
        // console.log("elements: ", elements);

        let formError = {};
        elements.forEach((ele) => {
          let mess;
          const { name, value, title, minLength, max, validity } = ele;

          if (minLength !== -1 && !value.length) {
            mess = `Vui lòng nhập thông tin ${title}`;
          } else if (value.length < minLength) {
            // validity.tooShort
            mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`;
          } else if (value.length > max && max) {
            mess = `Chỉ được nhập tối đa ${max} ký tự`;
          } else if (
            validity.patternMismatch &&
            ["id", "price"].includes(name) //?name chỉ có thể là id và price thì mới hợp lệ
          ) {
            mess = `Vui lòng nhập ký tự là số`;
          } else if (validity.patternMismatch && name === "email") {
            mess = `Vui lòng nhập đúng email`;
          }

          formError[name] = mess;
        });

        // console.log("formError: ", formError);
        let flag = false;
        //Kiểm tra xem nếu formError đang hđ?

        for (let key in formError) {
          // console.log('value: ', value);
          if (formError[key]) {
            flag = true;
            break;
          }
        }

        if (flag) {
          setFormError(formError);
          return;
        }

        if (productEdit) {
          dispatch(btFormActions.updateProduct(formData));
        } else {
          dispatch(btFormActions.addProduct(formData));
        }
      }}
      noValidate
    >
      <h2 className="px-2 py-4 bg-dark text-warning">Product Info</h2>
      <div className="form-group row">
        <div className="col-6">
          <p>ID</p>
          <input
            value={formData?.id}
            type="text"
            className="form-control"
            name="id"
            disabled={!!productEdit}
            title="id"
            onChange={handleFormData()}
            minLength={1}
            max={10}
            pattern="^[0-9]+$"
            // required
            // autoComplete="false"
          />
          <p className="text-danger">{formError?.id}</p>
        </div>
        <div className="col-6">
          <p>Image</p>
          <input
            value={formData?.image}
            type="text"
            className="form-control"
            // onChange={(ev) => {
            //   setFormData({
            //     ...formData,
            //     image: ev.target.value,
            //   });
            // }}
            name="image"
            title="image"
            minLength={1}
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.image}</p>
        </div>
      </div>

      <div className="form-group row">
        <div className="col-6">
          <p>Name</p>
          <input
            type="text"
            className="form-control"
            name="name"
            onChange={handleFormData()}
          />
        </div>
        <div className="col-6">
          <p>Product Type</p>
          <input
            type="text"
            className="form-control"
            name="type"
            onChange={handleFormData()}
          />
        </div>
      </div>

      <div className="form-group row">
        <div className="col-6">
          <p>price</p>
          <input
            type="text"
            className="form-control"
            name="price"
            onChange={handleFormData()}
            pattern="^[0-9]+$"
          />
          <p className="text-danger">{formError?.price}</p>
        </div>
        <div className="col-6">
          <p>Description</p>
          <input
            type="text"
            className="form-control"
            name="description"
            onChange={handleFormData()}
          />
        </div>
      </div>
      <div className="mt-3">
        {productEdit && <button className="btn btn-info ml-3">Update</button>}

        {!productEdit && <button className="btn btn-success">Create</button>}
      </div>
    </form>
  );
};

export default FormProduct;
