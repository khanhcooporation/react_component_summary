import React from "react";
import TableProduct from "./TableProduct";
import FormProduct from "./FormProduct";

const BTForm = () => {
  return (
    <div>
      <h1>BTForm</h1>
      <FormProduct />
      <TableProduct />
    </div>
  );
};

export default BTForm;
