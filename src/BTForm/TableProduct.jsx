import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormActions } from "../storeToolKit/BTForm/slice";

const TableProduct = () => {
  const { productList } = useSelector((state) => state.btForm);
  const dispatch = useDispatch();
  console.log("ProductList: ", productList);
  return (
    <div className="mt-5">
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Type</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {productList?.map((prd) => (
            <tr key={prd?.id}>
              <td>{prd?.id}</td>
              <td>{prd?.image}</td>
              <td>{prd?.name}</td>
              <td>{prd?.price}</td>
              <td>{prd?.description}</td>
              <td>{prd?.productType}</td>
              <td>
                <button
                  className="btn btn-success"
                  onClick={() => dispatch(btFormActions.deleteProduct(prd.id))}
                >
                  Delete
                </button>
                <button
                  className="btn btn-info ml-3"
                  onClick={() => dispatch(btFormActions.editProduct(prd))}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableProduct;
