//rafce
import React, { useState } from "react";
import ProductList from "./ProductList";
import data from "./data.json";
import ProductDetail from "./ProductDetail";
import GioHang from "./GioHang";

const BTPhones = () => {
  // chi tiết sản phẩm
  const [prdDetail, setPrdDetail] = useState(data[0]);

  // Danh sách giỏ hàng
  const [carts, setCarts] = useState([]);

  const handlePrdDetail = (product) => {
    setPrdDetail(product);
  };
  console.log("cart: ", carts);
  const handleCart = (product) => {
    console.log("handleCart ~ product:", product);
    // coppy những sản phẩm đang có ở trong cart và thêm vào 1 product mới
    // spread operator
    // setCarts([...carts, product])
    //Todo:Ktra trong giỏ hàng đã tồn tại sp hay chưa ? tăng số lượng: thêm sp
    const item = carts.find((prd) => prd.maSP == product.maSP);
    //? Find tìm thấy sẽ trả đúng item tmãn đk đó, else UNDEFINED
    //find(Tên đ diện , index)
    // const item = cart.find((prd) => {
    //   return prd.maSP==product.maSP;
    // });
    //? falsy - truethy
    //? falsy: undefined, NaN, 0, '', fale, null
    //? bỏ vào if(condition) thì sẽ tự chuyển thành BOOLEAN
    if (!item) {
      // ko tồn tại trong cart
      const newPrd = { ...product, soLuong: 1 };
      setCarts([...carts, newPrd]);
    } else {
      //đã tồn tại
      const newPrd = { ...item, soLuong: item.soLuong + 1 };
      //nôm na là tạo ptử mới ko chứa m tử item.maSP
      //cart[1,2,3,4]
      //newCart[1,2,3]
      const newCart = carts.filter((cart) => cart.maSP != item.maSP);
      setCarts([...newCart, newPrd]);
    }
    // setCarts([carts, { ...product, soLuong: 1 }]);
  };
  const handleQuantity = (maSP, quantity) => {
    console.log(maSP);
    console.log(quantity);
    setCarts((currentState) => {
      //*Kiểu render lại với quantity tăng lên
      const newCarts = currentState.map((cart) => {
        if (cart.maSP == maSP) {
          return {
            ...cart,
            // soLuong: cart.soLuong + quantity <= 1 ? 1 : cart.soLuong + quantity,
            soLuong: cart.soLuong + quantity || 1,
          };
        }
        return cart;
      });
      return newCarts;
    });
  };
  return (
    <div className="container mt-5">
      <div className="d-flex justify-content-between mb-5">
        <h1>BTPhones</h1>
        <button
          className="btn btn-danger"
          data-toggle="modal"
          data-target="#gioHang"
        >
          Giỏ hàng
        </button>
      </div>

      <ProductList
        data={data}
        handlePrdDetail={handlePrdDetail}
        handleCart={handleCart}
      />

      {/* Chi tiết sản phẩm */}
      <ProductDetail prdDetail={prdDetail} />

      {/* Giỏ hàng */}
      <GioHang handleQuantity={handleQuantity} carts={carts} />
    </div>
  );
};

export default BTPhones;
//!!!! Cai thu vien redux : npm i redux react-redux
