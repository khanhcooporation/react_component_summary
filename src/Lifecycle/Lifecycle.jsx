import axios from 'axios';
import React, { Component, createRef } from 'react'

export default class Lifecycle extends Component {
    //? LIFE CYCLE: chỉ có trong class component
    //3 giai đoạn
    //      1) MOUNTING: Các phương thức ở mounting sẽ được chạy khi component render ra UI
    //                   constructer: khởi tạo state và props và bind this vào các phương thức
    //                   getDerivedStateFromProps: Chạy sau hàm constructor và trước hàm render, dùng để thay đổi state trước khi render ra UI
    //                   render:  render ra UI
    //                   componentDidMount: Chạy sau hàm render thường được sử dụng để call API <=> useEffect() mảng dependency rỗng

    //      2) UPDATING: Chạy khi state hoặc Props thay đổi
    //  getDerivedStateFromProps: Thường để chuyển props thành state nội bộ của component
    //  shouldComponentUpdate: Dùng để quản lý việc re-render của Component
    //  render:
    //  getSnapshotBeforeUpdate: lấy giá trị DOM trước re-render
    //  ComponentDidupdateL Chạy khi component re-render (ko chạy ở lần render đầu tiên của component)
    //      => sử dụng để call api, tương tác với DOM <=> useEffect( () => {...dependencies}  )


    //      3) UNMOUNTING
    //  componentWillUnmount: chạy trước khi Component bị hủy khỏi UI 
    //  <=> 
    // useEffect( () => {
    //     return () => { }
    //     }, () )


    //TODO:     Mounting
    constructor(props) {
        super(props)
        this.state = {
            number: 1,
            movieList: [],
        }

        this.numberRef = createRef()

        this.intervalRef = createRef()

    }

    // state = {
    //     number: 1,
    // }

    // getInfo() {
    //     function sum() {
    //         console.log('this', this)
    //     }

    //     sum()

    //     const abc = () => {
    //         console.log('this', this)
    //     }

    //     abc()
    // }

    static getDerivedStateFromProps(nextProps, currentState) {
        console.log('currentState: ', currentState)

        // currentState.number = 10

        // if (nextProps.number !== currentState.number) {
        //     currentState.number = nextProps.number
        // }

        return currentState
    }

    shouldComponentUpdate() {
        // return false => component sẽ ko re-render
        // return true => component sẽ re-render

        return true
    }

    getSnapshotBeforeUpdate(prevProps, preState) {
        console.log('numberRef', this.numberRef.current.innerHTML)
        return null
    }

    componentDidUpdate() {
        console.log('componentDidUpdate: ');
        //tương tác DOM, call
    }

    componentDidMount() {
        console.log('componentDidmount')

        this.intervalRef.current = setInterval(() => {
            console.log('HELLO BC46');
        }, 1000)

            //call API
            ; (async () => {
                const data = await axios({
                    method: 'GET',
                    url: 'https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01',
                    headers: {
                        TokenCybersoft:
                            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NiIsIkhldEhhblN0cmluZyI6IjMxLzAxLzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcwNjY1OTIwMDAwMCIsIm5iZiI6MTY3ODI5NDgwMCwiZXhwIjoxNzA2ODA2ODAwfQ.RmFBx9ElL7VuYNzZnzMoGUHyC3iXKRpw7Yvq2LsXk0Q',
                    },
                })

                this.setState({
                    movieList: data?.data?.content,
                })
            })()
    }

    // <=> return function component
    render() {
        console.log('RENDER')

        const props = this.props

        console.log('props: ', props)

        console.log(this.state.movieList)
        // this.getInfo()

        return (
            <div>
                <h1>Lifecycle</h1>
                <p ref={this.numberRef}>Number {this.state.number}</p>
                <button
                    className="btn btn-success mt-3"
                    onClick={() => {
                        this.setState({
                            number: this.state.number + 1,
                        })
                    }}
                >
                    + Number
                </button>
            </div>
        )
    }
}


//! Render <=> return trong function component