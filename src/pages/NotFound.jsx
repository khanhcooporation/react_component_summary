import React from "react";
import { useNavigate } from "react-router-dom";

const NotFound = () => {
  const navigate = useNavigate();
  return (
    <div className="container">
      <h1 className="text-center">404NotFound</h1>
      <div className="text-center mt-5">
        <button
          className="btn btn-success"
          onClick={() => {
            navigate("/");
          }}
        >
          Home
        </button>
      </div>
    </div>
  );
};

export default NotFound;
