//rafce
import React from "react";
import { NavLink, Link } from "react-router-dom";
import { PATH } from "../config/path";

const HomePage = () => {
  return (
    <div className="container mt-5 d-flex flex-column">
      <Link to={PATH.databinding}>DataBinding</Link>
      <NavLink to={PATH.databindingcondition}>datAbindingwithcondition</NavLink>
      <NavLink to={PATH.handleEvent}>handleEvent</NavLink>
      <NavLink to={PATH.renderWithMap}>renderWithMap</NavLink>
      <NavLink to={PATH.btMovie}>btMovie</NavLink>
      <NavLink to={PATH.styleComponent}>styleComponent</NavLink>
      <NavLink to={PATH.demoState}>demoState</NavLink>
      <NavLink to={PATH.demoProps}>demoProps</NavLink>
      <NavLink to={PATH.btShoes}>btShoes</NavLink>
      <NavLink to={PATH.btPhones}>btPhones</NavLink>
      <NavLink to={PATH.redux}>demoRedux</NavLink>
      <NavLink to={PATH.btPhoneRedux}>btPhoneRedux</NavLink>
      <NavLink to={PATH.btDatVe}>btDatVe</NavLink>
      <NavLink to={PATH.btDatVeToolkit}>btDatVeToolkit</NavLink>
      {/* <NavLink to={PATH.btForm}>btForm</NavLink> */}
    </div>
  );
};

export default HomePage;
