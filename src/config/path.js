export const PATH = {
  about: "/about",
  databinding: "/databinding",
  databindingcondition: "/databindingwithcondition",
  handleEvent: "/handleEvent",
  renderWithMap: "/renderWithMap",
  btMovie: "/btMovie",
  movieDetail: "/btMovie/:movieId", //:id --> Params
  styleComponent: "/styleComponent",
  demoState: "/demoState",
  demoProps: "/demoProps",
  btShoes: "/btShoes",
  btPhones: "/btPhones",
  redux: "/redux/demoRedux",
  btPhoneRedux: "/redux/btPhoneRedux",
  btDatVeRedux: "/redux/btDatVe",
  btDatVeToolkit: "/redux/btDatVeToolkit",
  btForm: "/btForm",
  useEffect: "/useEffect",
  btTaskManager: "/btTaskManager",
  demoHooks: "/demoHooks",
  lifeCycle: "/lifeCycle"
};
