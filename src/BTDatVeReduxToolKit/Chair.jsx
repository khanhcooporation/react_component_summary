import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { baiTapDatVeActions } from "../storeToolKit/BTDatVe/slice";
import cn from "classnames";

const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  //    //?  v   Cái này lấy bên slice (initialState)
  const { chairBooking } = useSelector((state) => state.btDatVeToolkit); //Cai nay lay ben rootReducer
  //                          //?Dùng để lấy gtrị từ store (tức rootReducer)
  return (
    <button
      className={cn("btn btn-outline-dark Chair", {
        booking: chairBooking.find((e) => e.soGhe === ghe.soGhe),
      })}
      style={{ width: "50px" }}
      onClick={() => {
        dispatch(baiTapDatVeActions.setChairBooking(ghe));
      }}
    >
      {ghe.soGhe}
    </button>
  );
};

export default Chair;
