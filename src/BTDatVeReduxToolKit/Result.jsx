import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { baiTapDatVeActions } from "../storeToolKit/BTDatVe/slice";
import cn from "classnames";

const Result = () => {
  const { chairBooking, chairBooked } = useSelector(
    (state) => state.btDatVeToolkit
  );
  const dispatch = useDispatch();
  console.log(chairBooking);
  return (
    <div>
      <h2>Danh sách ghế bạn chọn</h2>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark">Ghế đã đặt</button>
        <button className="btn btn-outline-dark mt-3">Ghế đang chọn</button>
        <button className="btn btn-outline-dark mt-3">Ghế chưa đặt</button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {chairBooking.map((ghe) => (
            <tr key={ghe.soGhe}>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                <button
                  className={
                    (cn("btn btn-danger"),
                    {
                      booking: chairBooking.find((e) => e.soGhe == ghe.soGhe),
                      booked: chairBooked.find((e) => e.soGhe == ghe.soGhe),
                    })
                  }
                  onClick={() => {
                    dispatch: baiTapDatVeActions.setChairBooking(ghe);
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}
          <tr>
            <td>Tổng tiền</td>
            <td>
              {chairBooking.reduce((total, ghe) => (total += ghe.gia), 0)}
            </td>
          </tr>
        </tbody>
      </table>
      <button
        className="btn btn-success"
        onClick={() => {
          dispatch(baiTapDatVeActions.setChairBooked());
        }}
      >
        Thanh toán
      </button>
    </div>
  );
};

export default Result;
