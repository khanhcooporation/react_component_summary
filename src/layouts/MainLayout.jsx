import React from "react";
import Sidebar from "../components/Sidebar";
import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <div>
      <div className="row">
        {/* Side bar  */}
        <div className="col-2">
          <Sidebar></Sidebar>
        </div>
        {/* content */}
        <div className="col-10">
          <Outlet></Outlet>
          {/*//? Outlet: 1 tv của react RouterDom  */}
        </div>
      </div>
    </div>
  );
};

export default MainLayout;
