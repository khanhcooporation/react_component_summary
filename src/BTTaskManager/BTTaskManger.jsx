import axios from "axios";
import React, { useEffect, useState } from "react";
import cn from "classnames";
import { toast } from "react-toastify";
import { btTaskManagerServices } from "../services/btTaskManagerService";
const BTTaskManger = () => {
  const [taskList, setTaskList] = useState();

  const [formData, setFormData] = useState();

  const handleFormData = () => (ev) => {
    const { name, value } = ev.target;

    setFormData({
      ...formData,
      [name]: value,
    });
  };
  //*Await: đợi lấy API về trước xong mới chạy
  //*3 Cách xử lý bất đồng bộ: callback() -ES5, promise() -ES6, Async await - ES7
  //! Chỉ sd async await đối với 1 function xử lý bđb (await promise), tức ko thể sd trong 1 function bth
  //*Về bản chât Async, await là Promise => Return trong async await là một Promise
  const fetchTaskList = async () => {
    try {
      //Lấy data từ API
      const data = await axios.get(
        "http://svcy.myclass.vn/api/ToDoList/GetAllTask"
      );
      console.log("data: -------", data);
      setTaskList(data.data);
    } catch (err) {
      console.log(err);
    }
    return true;
  };

  //? IIFE (Immediately Invoked Function Expression):Một hàm được khơi tạo không tên và thực thi ngay tại thời điểm khởi tạo
  //?Thường sử dụng khi muốn gọi hàm async/await nhưng lại không có function bao ngoài
  //?- Khi gọi async/await trong useEffect React

  useEffect(() => {
    // fetchTaskList();

    //IIFE:
    (async () => {
      try {
        //Lấy data từ API
        const data = await axios.get(
          "http://svcy.myclass.vn/api/ToDoList/GetAllTask"
        );
        console.log("data: -------", data);
        setTaskList(data.data);
      } catch (err) {
        console.log(err);
      }
      return true;
    })(); //<========= Dấu gọi hàm, nôm na là anonymous function
  }, []);

  return (
    <div>
      <h1>BTTaskManager</h1>
      <form
        onSubmit={async (ev) => {
          try {
            // ngăn trình duyệt reload khi submit
            ev.preventDefault();

            // Call api tạo mới task
            // console.log(formData);
            // await axios.post(
            //   "http://svcy.myclass.vn/api/ToDoList/AddTask",
            //   formData
            // );
            await btTaskManagerServices.createTask(formData);
            //sau khi call thành công thì goi lại api fetch task list
            // const data = await axios.get(
            //   "http://svcy.myclass.vn/api/ToDoList/GetAllTask"
            // );
            const data = btTaskManagerServices.getTaskList();

            // sau khi có data từ api
            setTaskList(data.data);
            toast.success("Create task success!");
          } catch (err) {
            toast.error("Create task failed!");
            console.log(err);
          }
        }}
      >
        <input
          name="taskName"
          className="form-control"
          onChange={handleFormData()}
        />
        <div>
          <button className="btn btn-outline-success my-3">Create Task</button>
        </div>
      </form>

      {/* để render ra UI */}
      <table className="table">
        <thead>
          <tr>
            <th>Task Name</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {taskList?.map((task) => (
            <tr>
              <td>{task.taskName}</td>
              <td>
                <p
                  className={cn("badge", {
                    "badge-danger": !task?.status,
                    "badge-success": task.status,
                  })}
                >
                  {task?.status ? "Done" : "Inprocessing"}
                </p>
              </td>
              <td>
                {!task?.status && (
                  <button
                    className="btn btn-success"
                    onClick={async () => {
                      try {
                        // update status task => done
                        await axios.put(
                          `http://svcy.myclass.vn/api/ToDoList/doneTask?taskName=${task.taskName}`
                        );

                        // get lại danh sách task
                        const data = await axios.get(
                          "http://svcy.myclass.vn/api/ToDoList/GetAllTask"
                        );

                        // sau khi có data từ api
                        setTaskList(data.data);

                        toast.success("Update task success!");
                      } catch (err) {
                        toast.error("Update task error!");
                      }
                    }}
                  >
                    done
                  </button>
                )}
                <button
                  className="btn btn-danger ml-3"
                  onClick={async () => {
                    try {
                      //delTask
                      // await axios.delete(
                      //   `http://svcy.myclass.vn/api/ToDoList/deleteTask?taskName=${task.taskName}`
                      // );
                      await btTaskManagerServices.deleteTask(task.taskName);
                      //del thanh cong thi call api
                      // const data = await axios.get(
                      //   "http://svcy.myclass.vn/api/ToDoList/GetAllTask"
                      // );
                      const data = await axios.get(
                        "http://svcy.myclass.vn/api/ToDoList/GetAllTask"
                      );
                      // sau khi có data từ api
                      setTaskList(data.data);
                      toast.success("Delete task success!");
                    } catch (error) {
                      toast.error("Delete task success!");
                    }
                  }}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {/* <div> */}
      {/* {taskList?.map((task) => (
          <div
            className="d-flex"
            style={{
              gap: 20,
            }}
          >
            <p>{task?.taskName}</p>
            <p
              className={cn("badge", {
                "badge-danger": !task?.status,
                "badge-success": task.status,
              })}
            >
              {task?.status ? "Done" : "Inprocessing"}
            </p>
          </div>
        ))} */}

      {/* IIFE để render UI  */}
      {/* {(() => {
          const name = "abc";
          if (name) {
            return <div>{name}</div>;
          }
          return <div>NO NAME</div>;
        })()} */}
      {/* </div> */}
    </div>
  );
};

export default BTTaskManger;
