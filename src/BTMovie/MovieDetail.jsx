import React from "react";
import { useParams } from "react-router-dom";
import movieList from "./data.json";

const MovieDetail = () => {
  const params = useParams();
  const { movieId } = params;

  const renderMovieDetail = () => {
    const movie = movieList.find((value) => value.maPhim === movieId * 1);
    return (
      <div className="row">
        <div className="col-4">
          <img className="img-fluid" src={movie?.hinhAnh} alt="..." />
        </div>
        <div className="col-8">
          <p>{movie?.tenPhim}</p>
          <p>{movie?.moTa}</p>
        </div>
      </div>
    );
  };
  console.log("params: ", params);
  return <div>{renderMovieDetail()}</div>;
};

export default MovieDetail;

//optional chaining
