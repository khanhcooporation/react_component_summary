//rafce
import React, { useState } from "react";
import movieList from "./data.json";
import {
  useNavigate,
  generatePath,
  useSearchParams,
  useLocation,
} from "react-router-dom";
import { PATH } from "../config/path";
import { generateSearchString } from "../utils/generateSearchString";
import qs from "qs";
const BTMovie = () => {
  console.log({ movieList });
  const navigate = useNavigate();
  //*================================
  const [searchParams, setsearchParams] = useSearchParams();
  const { pathname } = useLocation();
  // lấy pathname
  const searchQuery = Object.fromEntries(searchParams);
  // console.log("searchQuery: ", searchQuery);
  // console.log("searchParams: ", searchParams.get("movieName"));
  //*=====================================

  const [searchValue, setSearchValue] = useState();
  console.log("searchValue: ", searchValue);

  const findMovieList = () => {
    const data = movieList.filter((item) =>
      item.tenPhim.toLowerCase().includes(searchQuery.movieName)
    );
    return data.length ? data : undefined;
    console.log("data: ", data);
  };

  const renderMovie = (arrMovie) => {
    return arrMovie.map((movie) => {
      const path = generatePath(PATH.movieDetail, {
        movieId: movie.maPhim,
      });
      return (
        <div key={movie.maPhim} className="col-3 mt-3">
          <div className="card">
            <img
              style={{ width: "250px", height: "350px" }}
              src={movie.hinhAnh}
              alt="..."
            />
            <div
              className="card-body"
              style={{ height: "300px", overflow: "hidden" }}
            >
              <p className="font-weight-bold">{movie.tenPhim}</p>
              <p>{movie.moTa.substring(0, 50)}...</p>
              <button
                className="btn btn-outline-success mt-3"
                onClick={() => {
                  navigate(path);
                }}
              >
                Detail
              </button>
            </div>
          </div>
        </div>
      );
    });
  };
  // findMovieList();

  const queryString = qs.stringify(
    {
      name: "abc",
      age: 20,
    },
    {
      addQueryPrefix: true,
    }
  );
  console.log(
    "🚀 ~ file: BTMovie.jsx:74 ~ BTMovie ~ queryString:",
    queryString
  );

  //? Nhận vào 1 Object và trả về String, nmà có option để chỉnh String mà ta trả về
  return (
    <div className="container">
      <h1>BTMovie</h1>
      <div>
        <input
          type="text"
          className="form-control"
          value={searchValue}
          defaultValue={searchQuery?.movieName}
          onChange={(event) => {
            //Lấy gtrị user nhập vào input
            //* console.log(event.target.value);
            setSearchValue(event.target.value);
          }}
        />
        <button
          className="btn btn-success mt-3"
          onClick={() => {
            const searchString = generateSearchString({
              movieName: searchValue,
            });
            // setsearchParams({
            //   movieName: searchValue,
            // });
            navigate(pathname + searchString);
          }}
        >
          Tìm kiếm
        </button>
      </div>
      <div className="row">
        {/* {movieList.map((movie) => {
          const path = generatePath(PATH.movieDetail, {
            movieId: movie.maPhim,
          });
          return (
            <div key={movie.maPhim} className="col-3 mt-3">
              <div className="card">
                <img
                  style={{ width: "250px", height: "350px" }}
                  src={movie.hinhAnh}
                  alt="..."
                />
                <div
                  className="card-body"
                  style={{ height: "300px", overflow: "hidden" }}
                >
                  <p className="font-weight-bold">{movie.tenPhim}</p>
                  <p>{movie.moTa.substring(0, 50)}...</p>
                  <button
                    className="btn btn-outline-success mt-3"
                    onClick={() => {
                      navigate(path);
                    }}
                  >
                    Detail
                  </button>
                </div>
              </div>
            </div>
          );
        })} */}
        {renderMovie(searchQuery?.movieName ? findMovieList() : movieList)}
        {/* Nếu có tìm kiếm key là movieName  */}
      </div>
    </div>
  );
};

export default BTMovie;
