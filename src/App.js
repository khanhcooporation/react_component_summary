// import logo from './logo.svg'
// import DemoFunctionComponent from './components/DemoFunctionComponent'
// import DemoClassComponent from './components/DemoClassComponent'
import { Route, Routes } from "react-router-dom";
import "./App.css";
import Home from "./BTComponent/Home";
import BTDatVe from "./BTDatVe/BTDatVe";
import BTDatVeToolkit from "./BTDatVeReduxToolKit/BTDatVeToolkit";
import BTMovie from "./BTMovie/BTMovie";
import BTPhoneRedux from "./BTPhoneRedux/BTPhoneRedux";
import BTPhones from "./BTPhones/BTPhones";
import BTShoes from "./BTShoes/BTShoes";
import DataBindingWithCondition from "./DataBindingWithCondition/DataBindingWithCondition";
import DataBinding from "./Databinding/DataBinding";
import DemoProps from "./DemoProps/DemoProps";
import DemoRedux from "./DemoRedux/DemoRedux";
import HandleEvent from "./HandleEvent/HandleEvent";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoState from "./state/DemoState";
import StyleComponent from "./styleComponent/StyleComponent";
// import HomePage from "./pages/HomePage";
// import About from "./pages/About";
import { PATH } from "./config/path";
import MainLayout from "./layouts/MainLayout";
import MovieDetail from "./BTMovie/MovieDetail";
import NotFound from "./pages/NotFound";
import BTForm from "./BTForm/BTForm";
import DemoUseEffect from "./hooks/useEffect/DemoUseEffect";
import BTTaskManger from "./BTTaskManager/BTTaskManger";
import DemoHooks from "./hooks/demoHooks/DemoHooks";
import Lifecycle from "./Lifecycle/Lifecycle";
// JSX: Javascript XML => cho phép các bạn viết html 1 cách dễ dàng trong js
// binding:
// attributes: viết theo quy tắc camelCase

// Component: thành phần nhỏ giao diện ứng dụng
// 2 loại component: function component (stateLess component), class component (stateFull component) lifecicle

function App() {
  // const name = 'Nguyễn Viết Hải'
  // const sum = (a, b) => a + b
  return (
    <div>
      <Routes>
        <Route element={<MainLayout></MainLayout>}>
          {/* //*EX: */}
          {/* <Route index element={<HomePage />} />
          <Route path="/about" element={<About />} /> */}
          {/* //*================================================ */}
          {/* <Route
          path="/demoFunctionComponent"
          element={<DemoFunctionComponent />}
        /> */}
          {/* <Route path="/demoClassComponent" element={<DemoClassComponent />} /> */}
          <Route path="/Home" element={<Home />} />
          <Route index element={<DataBinding />} />
          <Route
            path={PATH.databindingcondition}
            element={<DataBindingWithCondition />}
          />
          <Route path={PATH.handleEvent} element={<HandleEvent />} />
          <Route path={PATH.renderWithMap} element={<RenderWithMap />} />
          <Route path={PATH.btMovie} element={<BTMovie />} />
          <Route
            path={PATH.movieDetail}
            element={<MovieDetail></MovieDetail>}
          ></Route>
          <Route path={PATH.styleComponent} element={<StyleComponent />} />
          <Route path={PATH.demoState} element={<DemoState />} />
          <Route path={PATH.demoProps} element={<DemoProps />} />
          <Route path={PATH.btShoes} element={<BTShoes />} />
          <Route path={PATH.btPhones} element={<BTPhones />} />
          <Route path={PATH.btForm} element={<BTForm />}></Route>
          <Route path={PATH.useEffect} element={<DemoUseEffect />}></Route>
          <Route path={PATH.btTaskManager} element={<BTTaskManger />}></Route>
          <Route path={PATH.demoHooks} element={<DemoHooks />}></Route>
          <Route path={PATH.lifeCycle} element={<Lifecycle />}></Route>
          {/* <Route path="/demoRedux" element={<DemoRedux />} s/> */}
          {/* <Route path="/btPhoneRedux" element={<BTPhoneRedux />} /> */}
          {/* <Route path="/btDatVe" element={<BTDatVe />} /> */}
          {/* <Route path="/btDatVeToolkit" element={<BTDatVeToolkit />} /> */}
          {/* //*NESTED ROUTER  */}
          <Route path="/redux">
            <Route index element={<DemoRedux />} />
            <Route path={PATH.btPhoneRedux} element={<BTPhoneRedux />} />
            <Route path={PATH.btDatVeRedux} element={<BTDatVe />} />
            <Route path={PATH.btDatVeToolkit} element={<BTDatVeToolkit />} />
          </Route>
        </Route>
        <Route path="*" element={<NotFound/>} />
        {/* //* đường dẫn k tồn tại sẽ vào đây */}
      </Routes>
    </div>
  );
}

export default App;
