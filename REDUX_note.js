/**
 * todo:     useSelector( state => rootReducer)
 *  ex: rootReducer:
 *          export const rootReducer = combineReducers({
 *          demoRedux: demoReduxReducer,
 *          })
 *      const demoRedux = useSelector((state) => state.demoRedux)
 *
 * *rootReducer dùng để chứa các reducer và STORE dùng để chứa toàn bộ những reducer đó
 * *Từng reducer là một useState; ta sd useState đó thông qua useSelector
 * *dispatch({}) dùng để thay đôi state
 */
