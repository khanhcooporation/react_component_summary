//?npm i react-router-dom
//    Tạo react rouderdom (nhớ vô package xem)

//?B2:import { BrowserRouter } from "react-router-dom";

//?B3: Wrap:
<BrowserRouter>
  <App />
</BrowserRouter>;

//?B4: Tao folder pages
//*                     => Home.jsx => rafce
//*                     => About.jsx

//? B5:
import { Route, Routes } from "react-router-dom";
import Home from "./pages/HomePage";
import About from "./pages/About";

<Routes>
  //!nếu để vầt HomePage sẽ luôn làm trang chủ
  <Route index element={<HomePage />} />
  //!=================================
  <Route path="/home" element={<Home />}></Route>
  <Route path="/about" element={<About />} />
</Routes>;

//?B6: Dẫn đến 1 trang
<NavLink to="/databinding">DataBinding</NavLink>;

//*NESTED ROUTER
<Route path="/redux">
  <Route path="/redux/demo" element={<DemoRedux />} />
  <Route path="/redux/btPhoneRedux" element={<BTPhoneRedux />} />
  <Route path="/redux/btDatVe" element={<BTDatVe />} />
  <Route path="/redux/btDatVeToolkit" element={<BTDatVeToolkit />} />
</Route>;

///TODO:==================================================
//TODO: Outlet:
//* App.js
<Routes>
  <Route element={<MainLayout></MainLayout>}>
    {/* Route1
    Route2 
    Route3 */}
  </Route>
</Routes>;

//*MainLayout
import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <div>
      <div className="row">
        {/* Side bar  */}
        <div className="col-2">
          <Sidebar></Sidebar>
        </div>
        {/* content */}
        <div className="col-10">
          <Outlet></Outlet>
        </div>
      </div>
    </div>
  );
};

export default MainLayout;

//!=>>> Outlet sẽ thay đổi dựa trên Route123

//TODO: useNavigte()      (react RouterDom)
// Dùng để chuyển trang cho button
//? Mỗi lần chuyển sẽ reload lại toàn bộ trang
//TODO: generatePath( link,  {param: ?}  )
// ex:  movieDetail: "/btMovie/:id", //:id --> Params
// generatePath("/btMovie/:id" , {id=1} );

//TODO: useLocation();
// trả về pathname của trang

//TODO: QueryURL: sau '?' tren URL
//*http://localhost:3000/btMovie?movieName=inside&age=20

//TODO:  useSearchParams();
const [searchParams, setsearchParams] = useSearchParams();
// => Một dạng của use State
// => tham số 1:
// => tham số 2:
const searchQuery = Object.fromEntries(searchParams);
console.log("searchQuery: ", searchQuery);
//? =>Sẽ in ra 1 object chứa all key trên url

console.log("searchParams: ", searchParams.get("movieName"));
//? =>Get 1 key cụ thể trên url (movieName)

//Todo: Event param (kiểu 1 thư viện để lấy dữ liệu nhập vào)
<input
  type="text"
  className="form-control"
  onChange={(event) => {
    //Lấy gtrị user nhập vào input
    console.log(event.target.value);
  }}
/>;
//ko cần enter, nó sẽ tự track mỗi khi nhập (XỊN VÃI CẶC)

//TODO: npm i qs
//? Nhận vào 1 Object và trả về String, nmà có option để chỉnh String mà ta trả về
import qs from "qs";
const queryString = qs.stringify({
  name: "abc",
  age: 20,
});
//* KQ: name=abc&age=20
const queryStrin1 = qs.stringify({
  name: "abc",
  age: undefined,
  year: 123,
});
//* KQ: name=abc&year=123

const queryStringPRE = qs.stringify(
  {
    name: "abc",
    age: 20,
  },
  {
    addQueryPrefix: true,
  }
);
//* KQ: ?name=abc&year=123

//TODO:   FORM ON SUBMIT
<form
  onSubmit={(event) => {
    //*Ngăn sự kiện reload trang khi submit
    event.preventDefault();
    console.log("submit");
  }}
></form>;

//Todo: required
//để force ng nhập input
//* tắt popup: autoComplete="false"

//todo: npm i axios
